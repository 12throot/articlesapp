//
//  ListServiceType.swift
//  TestableApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation

import ArticlesAppServices


////////////////////////////////////////////////////////////////////////////////////////////////////
internal protocol ListServiceType {
    
    func getItems() -> [ListViewItem]
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////
internal class ListService: ListServiceType {
    
    
    internal func getItems() -> [ListViewItem] {
        return []
    }
    
}
