//
//  DetailsTests.swift
//  ArticlesApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import XCTest

@testable import ArticlesApp


////////////////////////////////////////////////////////////////////////////////////////////////////
class DetailsTests: XCTestCase {
    
    var view: DetailViewType!
    var service: DetailServiceType!
    var viewModel: DetailViewModel!
    var item: DetailViewItem!
    var user: String!
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Setup & Teardown
    
    override func setUp() {
        super.setUp()
        
        self.view = DetailViewStub()
        self.service = DetailServiceMock()
        self.item = (ID: "SQhD", imagePath: "image-path", title: "fake item title", contents: "Lorem Ipsum")
        (self.service as! DetailServiceMock).item = self.item
        self.viewModel = DetailViewModel(ID: self.item.ID, view: self.view, service: self.service)
        self.user = "test-user"
    }
    
    
    override func tearDown() {
        self.view = nil
        self.service = nil
        self.item = nil
        self.viewModel = nil
        super.tearDown()
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Init
    
    func test_CanSetUpSuite() {
        XCTAssert(self.view != nil)
        XCTAssert(self.service != nil)
        XCTAssert(self.viewModel != nil)
        XCTAssert(self.user != nil)
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Logic
    
    
    func test_whenLoaded_updateItem() {
        self.viewModel.input.reload()
        
        XCTAssert(self.viewStub.updateCalled.count > 0)
    }
    
    
    func test_favorteTappedAndUserIsAuth_favoriteIsUpdated() {
        self.viewModel.reload()
        
        self.serviceMock.setUser(self.user)
        self.viewModel.input.favoriteTapped()
        
        
        XCTAssert(self.viewStub.setFavoriteCalled.count == 1)
    }
    
    
    func test_favorteAndUserIsAuthAndAlreadyFavorited_favoriteIsUpdated() {
        self.viewModel.reload()
        
        self.serviceMock.setUser(self.user)
        self.serviceMock.addToFavorites(self.item.ID)
        
        self.viewModel.input.favoriteTapped()
        
        XCTAssert(self.viewStub.setFavoriteCalled[0] == false)
    }
    
    
    func test_favoriteTapped_successfulLogin_favoriteUpdate() {
        self.viewModel.reload()
        
        self.viewModel.input.favoriteTapped()
        
        XCTAssert(self.viewStub.routeToLoginCalled.count == 1)
        
        self.serviceMock.setUser(self.user)
        
        self.viewModel.reload()
        
        self.viewModel.input.favoriteTapped()
        
        XCTAssert(self.viewStub.setFavoriteCalled.count == 1)
        XCTAssert(self.viewStub.setFavoriteCalled[0] == true)
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Routing
    
    func test_whenFavorteTappedAndUserNotLoggedIn_routeToLogin() {
        self.viewModel.reload()
        
        self.viewModel.input.favoriteTapped()
        
        XCTAssert(self.viewStub.routeToLoginCalled.count == 1)
    }
    
    
    func test_whenFavorteTappedAndUserIsLoggedIn_dontRouteToLogin() {
        self.viewModel.reload()
        
        self.serviceMock.setUser(self.user)
        self.viewModel.input.favoriteTapped()
        
        XCTAssert(self.viewStub.routeToLoginCalled.count == 0)
    }
    
    
    func test_whenImageTapped_routeToImage() {
        self.viewModel.reload()
        
        self.viewModel.input.imageTapped()
        
        XCTAssert(self.viewStub.routeToImageCalled.count == 1)
    }
    
    
    func test_favoriteTapped_unsuccessfulLogin_routeAgain() {
        self.viewModel.reload()
        
        self.viewModel.input.favoriteTapped()
        
        XCTAssert(self.viewStub.routeToLoginCalled.count == 1)
        
        self.viewModel.reload()
        
        self.viewModel.input.favoriteTapped()
        
        XCTAssert(self.viewStub.setFavoriteCalled.count == 0)
        XCTAssert(self.viewStub.routeToLoginCalled.count == 2)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Util
    
    private var viewStub: DetailViewStub {
        return self.view as! DetailViewStub
    }
    
    
    private var serviceMock: DetailServiceMock {
        return self.service as! DetailServiceMock
    }
}
