//
//  ListViewType.swift
//  Framework
//
//  Created by Kamil Swojak on 08/08/2017.
//
//

import Foundation


////////////////////////////////////////////////////////////////////////////////////////////////////
public typealias ListViewItem = (ID: String, imagePath: String, title: String)


////////////////////////////////////////////////////////////////////////////////////////////////////
public protocol ListViewType {
    
    func updateItems(_ value: [ListViewItem])
    
    func noResults()
    
    func route(_ path: String)
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////
extension ListViewType {
    
    
    public func loadViewModel() -> ListViewModel {
        return ListViewModel(view: self, service: ListService())
    }
    
}

