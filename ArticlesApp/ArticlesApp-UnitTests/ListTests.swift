//
//  ListTests.swift
//  TestableApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import XCTest


@testable import ArticlesApp


////////////////////////////////////////////////////////////////////////////////////////////////////
class ListTests: XCTestCase {
    
    var viewModel: ListViewModel!
    
    var view: ListViewStub!
    var service: ListServiceType!
    
    let mockData: [ListViewItem] = [ (ID: "0", imagePath: "first-image", title: "First Item"),
                                     (ID: "1", imagePath: "second-image", title: "Second Item")]
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Setup & Teardown
    
    override func setUp() {
        super.setUp()
        
        self.view = ListViewStub()
        self.service = ListServiceMock()
        (self.service as! ListServiceMock).data = self.mockData
        self.viewModel = ListViewModel(view: self.view, service: self.service)
    }
    
    
    override func tearDown() {
        self.view = nil
        self.service = nil
        self.viewModel = nil
        
        super.tearDown()
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Init
    
    func test_CanSetUpSuite() {
        XCTAssert(self.view != nil)
        XCTAssert(self.service != nil)
        XCTAssert(self.viewModel != nil)
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Logic
    
    func test_WhenViewDidLoadWithItems_UpdateList() {
        self.viewModel.reload()
        
        XCTAssert(self.view.updateItemsCalled == true)
        XCTAssert(self.view.noResultsCalled == false)
    }
    
    
    func test_WhenViewDidLoadWithoutItems_CallNoResults() {
        self.loadViewModelWithoutItems()
        self.viewModel.reload()
        
        XCTAssert(self.view.updateItemsCalled == false)
        XCTAssert(self.view.noResultsCalled == true)
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Routing
    
    func test_WhenItemSelected_Route() {
        self.viewModel.reload()
        
        let index = 0
        
        self.viewModel.didSelectedItem(at: index)
        
        XCTAssert(self.view.routeCalled == true)
    }
    
    
    func test_WhenItemSelected_RouteUsingCorrectID() {
        self.viewModel.reload()
        
        let index = 1
        let item = self.mockData[index]
        
        self.viewModel.didSelectedItem(at: index)
        
        XCTAssert(self.view.routeCalledValues[0] == item.ID)
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Errors
    
    func test_WhenSelectedWithInvalidIndex_NoCrash() {
        self.viewModel.reload()
        
        self.viewModel.didSelectedItem(at: -11)
        self.viewModel.didSelectedItem(at: 9001)
        
        XCTAssert(true)
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Util
    
    fileprivate func loadViewModelWithoutItems() {
        (self.service as! ListServiceMock).data = []
        self.viewModel = ListViewModel(view: self.view, service: self.service)
    }
}
