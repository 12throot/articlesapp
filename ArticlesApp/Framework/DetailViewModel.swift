//
//  DetailViewModel.swift
//  ArticlesApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation


////////////////////////////////////////////////////////////////////////////////////////////////////
// Wanted to give kickstarter idea a go, but if we don't use rx for this I'm not sure it makes sense to have UIViews implement ViewModelOutputs - this doesn't really give any information as to what would be expected behaviour of such protocol

////////////////////////////////////////////////////////////////////////////////////////////////////
protocol DetailViewModelInputs {
    
    func reload()
    
    func favoriteTapped()
    
    func imageTapped()
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////
public class DetailViewModel: DetailViewModelInputs {
    
    let view: DetailViewType
    let service: DetailServiceType
    var ID: String
    var item: DetailViewItem!
    
    var input: DetailViewModelInputs {
        return self
    }
    
    
    init(ID: String, view: DetailViewType, service: DetailServiceType) {
        self.view = view
        self.service = service
        self.ID = ID
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Inputs
    
    
    func reload() {
        self.item = self.service.getDetails(ID)
        
        self.onFinishedLoading()
    }
    
    
    func favoriteTapped() {
        guard self.service.isLoggedIn() else {
            self.view.routeToLogin()
            return
        }
        
        if self.service.isFavorite(self.ID) {
            self.service.removeFavorite(self.ID)
        }
        else {
            self.service.addToFavorites(self.ID)
        }
        self.view.setFavoriteState(self.service.isFavorite(ID))
    }
    
    
    func imageTapped() {
        self.view.routeToImage()
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Private
    
    func onFinishedLoading() {
        self.view.update(self.item)
    }
    
    
}
