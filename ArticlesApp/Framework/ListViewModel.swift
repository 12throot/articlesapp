//
//  ListViewModel.swift
//  TestableApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation


////////////////////////////////////////////////////////////////////////////////////////////////////
public class ListViewModel {
    
    private let view: ListViewType
    private let listService: ListServiceType
    private var listItems: [ListViewItem]!
    
    
    internal init(view: ListViewType, service: ListServiceType) {
        self.view = view
        
        self.listService = service
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Public
    
    public func reload() {
        let items = self.listService.getItems()
        
        self.onFinishedLoadWithItems(items)
    }
    
    
    public func didSelectedItem(at index: Int) {
        guard index >= 0 && index < self.listItems.count else {
            return
        }
        
        self.view.route(self.listItems[index].ID)
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Private
    
    private func onFinishedLoadWithItems(_ data: [ListViewItem]) {
        self.listItems = data
        
        if self.listItems.count == 0 {
            self.view.noResults()
        }
        else {
            self.view.updateItems(self.listItems)
        }
    }
    
}
