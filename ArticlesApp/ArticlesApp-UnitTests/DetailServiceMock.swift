//
//  DetailServiceMock.swift
//  ArticlesApp
//
//  Created by Kamil Swojak on 10/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation

@testable import ArticlesApp


////////////////////////////////////////////////////////////////////////////////////////////////////
class DetailServiceMock: DetailServiceType {
    
    typealias User = String
    
    
    var item: DetailViewItem! = nil
    var favorites: Set<String> = Set<String>()
    var user: User?
    
    
    func getDetails(_ ID: String) -> DetailViewItem {
        return item
    }
    
    
    func isFavorite(_ ID: String) -> Bool {
        return self.favorites.contains(ID)
    }
    
    
    func addToFavorites(_ ID: String) {
        self.favorites.insert(ID)
    }
    
    
    func removeFavorite(_ ID: String) {
        self.favorites.remove(ID)
    }
    
    
    func isLoggedIn() -> Bool {
        return self.user != nil
    }
    
    
    func setUser(_ value: User) {
        self.user = value
    }
}
