//
//  ArticlesAuthService.swift
//  ArticlesApp
//
//  Created by Kamil Swojak on 10/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation


////////////////////////////////////////////////////////////////////////////////////////////////////
public class ArticlesAuthService {
    
    private var isLoggedIn: Bool = false
    private var user: User?
    
    
    public init() {}
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: Public
    
    public func logInWithUser(_ user: User) {
        self.user = user
        self.isLoggedIn = true
    }
    
    
    public func logOut() {
        self.user = nil
        self.isLoggedIn = false
    }
    
}
