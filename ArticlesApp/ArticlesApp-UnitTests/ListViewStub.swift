//
//  ListViewStub.swift
//  ArticlesApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation

@testable import ArticlesApp


////////////////////////////////////////////////////////////////////////////////////////////////////
class ListViewStub: ListViewType {
    
    var updateItemsCalled: Bool = false
    var updateItemsCalledValues: [[ListViewItem]] = []
    var noResultsCalled: Bool = false
    var routeCalled: Bool = false
    var routeCalledValues: [String] = []
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: LitsViewType
    
    func updateItems(_ value: [ListViewItem]) {
        self.updateItemsCalled = true
        self.updateItemsCalledValues.append(value)
    }
    
    
    func noResults() {
        self.noResultsCalled = true
    }
    
    
    func route(_ path: String) {
        self.routeCalled = true
        self.routeCalledValues.append(path)
    }
}
