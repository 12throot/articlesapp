//
//  DetailViewService.swift
//  Framework
//
//  Created by Kamil Swojak on 08/08/2017.
//
//

import Foundation


////////////////////////////////////////////////////////////////////////////////////////////////////
public class ArticleDetailService {
    
    
    public init() {}
    
    
    public func getDetails(ID: String) -> Article {
        return ArticleDatabase.allArticles[ID]!
    }
    
}


