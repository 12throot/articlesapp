//
//  DetailService.swift
//  ArticlesApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation

import ArticlesAppServices


////////////////////////////////////////////////////////////////////////////////////////////////////
internal protocol DetailServiceType {
    
    func getDetails(_ ID: String) -> DetailViewItem
    
    func isFavorite(_ ID: String) -> Bool
    
    func addToFavorites(_ ID: String)
    
    func removeFavorite(_ ID: String)
    
    func isLoggedIn() -> Bool
    
    func setUser(_ value: User)
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////
internal class DetailService {
    
    let articleDetailService = ArticleDetailService()
    var favoriteArticlesService: FavoriteArticlesService?
    var authService = ArticlesAuthService()
    
    var user: User?
    
    
    internal init(user: User?) {
        self.user = user
    }
    
    
    public func setUser(_ value: User) {
        self.user = value
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////
extension DetailService: DetailServiceType {
    
    func isLoggedIn() -> Bool {
        return nil != self.user
    }

    
    func getDetails(_ ID: String) -> DetailViewItem {
        let article = self.articleDetailService.getDetails(ID: ID)
        
        return (ID: article.ID,
                imagePath: article.imagePath,
                title: article.title,
                contents: article.contents)
    }
    
    
    func isFavorite(_ ID: String) -> Bool {
        return false
    }
    
    
    func addToFavorites(_ ID: String) {
        
    }
    
    
    func removeFavorite(_ ID: String) {
        
    }
    
}
