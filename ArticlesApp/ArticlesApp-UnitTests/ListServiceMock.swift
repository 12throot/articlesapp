//
//  ListViewMock.swift
//  ArticlesApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation

@testable import ArticlesApp


////////////////////////////////////////////////////////////////////////////////////////////////////
class ListServiceMock: ListServiceType {
    
    var data: [ListViewItem] = []
    
    
    func getItems() -> [ListViewItem] {
        return self.data
    }
}



