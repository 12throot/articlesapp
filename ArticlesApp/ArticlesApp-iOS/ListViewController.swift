//
//  ListViewController.swift
//  TestableApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import UIKit

import ArticlesApp


////////////////////////////////////////////////////////////////////////////////////////////////////
class ListViewController: UIViewController, ListViewType {
    
    private var viewModel: ListViewModel!
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel = (self as ListViewType).loadViewModel()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel.reload()
    }
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// MARK: -
// MARK: ListViewType
extension ListViewController {
    
    func updateItems(_ value: [ListViewItem]) {
        print("Updated with values:", value.count)
    }
    
    
    func noResults() {
        print("NoResults")
    }
    
    
    func route(_ path: String) {
        print("Route with path:", path)
    }
    
}
