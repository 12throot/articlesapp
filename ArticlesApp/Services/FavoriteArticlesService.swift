//
//  FavoriteService.swift
//  Framework
//
//  Created by Kamil Swojak on 08/08/2017.
//
//

import Foundation


////////////////////////////////////////////////////////////////////////////////////////////////////
public class FavoriteArticlesService {
    
    private var favorites: [String] = []
    private let user: User
    
    
    public init(user: User){
        self.user = user
    }
    
    
    public func isFavorite(_ ID: String) -> Bool {
        return self.favorites.contains(ID)
    }
    
    
    func addToFavorites(_ ID: String) {
        self.favorites.append(ID)
    }
    
    
    func removeFavorite(_ ID: String) {
        guard let index = self.favorites.index(of: ID) else {
            return
        }
        
        self.favorites.remove(at: index)
    }
    
    
    public func getFavoriteIDs() -> [String] {
        return self.favorites
    }
}
