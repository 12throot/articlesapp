//
//  ViewController.swift
//  TestableApp-iOS
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import UIKit

import ArticlesApp


////////////////////////////////////////////////////////////////////////////////////////////////////
class ViewController: UIViewController {
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: -
    // MARK: UIViewController
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.present(ListViewController(), animated: false)
    }
    
}
