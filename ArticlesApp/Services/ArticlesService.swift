//
//  ItemListService.swift
//  Framework
//
//  Created by Kamil Swojak on 09/08/2017.
//
//

import Foundation


////////////////////////////////////////////////////////////////////////////////////////////////////
public class ArticlesService {
    
    public init() {}
    
    
    public func getArticles() -> [ArticleHeader] {
        return ArticleDatabase
            .allArticles.values
            .map { (article: Article) -> ArticleHeader in
                return ArticleHeader(ID: article.ID, imagePath: article.imagePath, title: article.title)
        }
    }
}


