//
//  File.swift
//  Framework
//
//  Created by Kamil Swojak on 08/08/2017.
//
//

import Foundation


////////////////////////////////////////////////////////////////////////////////////////////////////
public struct Article {
    public var ID: String
    public var imagePath: String
    public var title: String
    public var contents: String
}
