//
//  DetailViewType.swift
//  ArticlesApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation

////////////////////////////////////////////////////////////////////////////////////////////////////
public typealias DetailViewItem = (ID: String, imagePath: String, title: String, contents: String)


////////////////////////////////////////////////////////////////////////////////////////////////////
public protocol DetailViewType {
    
    func update(_ value: DetailViewItem)
    
    func setFavoriteState(_ value: Bool)
    
    func routeToLogin()
    
    func routeToImage()
    
}

