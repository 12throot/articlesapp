//
//  DetailViewStub.swift
//  ArticlesApp
//
//  Created by Kamil Swojak on 10/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation

@testable import ArticlesApp


////////////////////////////////////////////////////////////////////////////////////////////////////
class DetailViewStub: DetailViewType {
    
    var updateCalled: [DetailViewItem] = []
    var setFavoriteCalled: [Bool] = []
    var routeToLoginCalled: [Void] = []
    var routeToImageCalled: [Void] = []
    
    
    func update(_ value: DetailViewItem) {
        self.updateCalled.append(value)
    }
    
    
    func setFavoriteState(_ value: Bool) {
        self.setFavoriteCalled.append(value)
    }
    
    
    func routeToLogin() {
        self.routeToLoginCalled.append(())
    }
    
    
    func routeToImage() {
        self.routeToImageCalled.append(())
    }
}
