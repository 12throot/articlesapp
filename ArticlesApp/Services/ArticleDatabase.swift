//
//  Model.swift
//  TestableApp
//
//  Created by Kamil Swojak on 09/08/2017.
//  Copyright © 2017 Kamil Swojak. All rights reserved.
//

import Foundation

////////////////////////////////////////////////////////////////////////////////////////////////////
struct ArticleDatabase {
    
    static let allArticles: [String : Article] = articles
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////
fileprivate let articles: [String : Article] = [
    
    "AQiw" : Article(ID: "AQiw",
            imagePath: "https://i2.wp.com/vitalpethealth.co.uk/wp-content/uploads/2014/09/Puppy-1.jpg?fit=200%2C213",
            title: "Ban pet shop puppy and kitten sales?",
            contents: "A change in UK legislation is being discussed in Parliament, concerning the sale and passage of young puppies and kittens. The sale and passage of pets in pet shops (which are licenced and regulated) is currently controlled under The 1951 Pets and Animals Act."),
    
    "Qs5iqW" : Article(ID: "Qs5iqW",
            imagePath: "https://i2.wp.com/vitalpethealth.co.uk/wp-content/uploads/2016/11/bob.jpg?fit=295%2C475",
            title: "A Street Cat Named Bob-A must see movie",
            contents: "Based on the true events novel by James Bowen, this feel good story has been beautifully brought to life on the big screen. We highly recommend getting to your local cinema to watch this heart rendering, awe-inspiring movie."),
    
    "OpwdEs" : Article(ID: "OpwdEs",
            imagePath: "https://i0.wp.com/vitalpethealth.co.uk/wp-content/uploads/2016/11/shutterstock_75208735_christmas.jpg?fit=800%2C569",
            title: "Winter Holiday Pet Poison Awareness",
            contents: "It is that time of year when we start thinking about putting up our Christmas tree, hanging mistletoe and drinking mulled wine. But did you know the Christmas holidays are one of the most busy times of year for Veterinary services? This is mostly down to our pets ingesting seasonal plants, food and decorations which are toxic to them. Here are the main Christmas culprits and how to avoid a poorly pooch or puss over the holiday period.")
]
